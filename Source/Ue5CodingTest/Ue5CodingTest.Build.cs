// Copyright (C) 2022 Humanitas Solutions. All rights reserved.

using UnrealBuildTool;

public class Ue5CodingTest : ModuleRules
{
  public Ue5CodingTest(ReadOnlyTargetRules Target) : base(Target)
  {
    PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

    PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
  }
}
