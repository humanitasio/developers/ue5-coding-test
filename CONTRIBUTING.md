# Contributing

## Setting up my environment

The [Environment](https://humanitasio.gitlab.io/hxs-wiki-public/docs/developers/environments/) section of [HXS Wiki](https://humanitasio.gitlab.io/hxs-wiki-public) covers everything you needs to know to get you machine ready.

## Collaborating with Git

The [Source Code Management](https://humanitasio.gitlab.io/hxs-wiki-public/docs/developers/source-code-management/) section of [HXS Wiki](https://humanitasio.gitlab.io/hxs-wiki-public) will help you get ready with Git.

## Developing in Unreal Engine

Before contributing to the project, make sure to read the general guidelines regarding [Project Directory Structure](https://humanitasio.gitlab.io/hxs-wiki-public/docs/developers/project-directory-struture/#unreal-engine-project) and [Coding Style](https://humanitasio.gitlab.io/hxs-wiki-public/docs/developers/coding-style/#unreal-engine-project).
