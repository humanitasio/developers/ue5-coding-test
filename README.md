# UE5 Coding Test

Coding test to assess candidates' knowledge and skills for [Unreal Engine](https://www.unrealengine.com) C++ development

## Getting Started

In this test you will develop a new UE5 plugin and add it to the _UE5 Coding Test_ project.

### Request access

During this test you will need access to some in-house resources and documentation.

Before starting the test, you need to make sure you have access to the following resources:

- [UE5 Coding Test](https://gitlab.com/humanitasio/developers/ue5-coding-test) repository
- [UE5 Plugin Example](https://gitlab.com/humanitasio/simulator/ue5-plugin-example) repository
- [HXS Wiki](https://humanitasio.gitlab.io/hxs-wiki-public/) website

### Fork the project

You will not be able to push any changes to the original _UE5 Coding Test_ repository. To be able to work on this project, you will have to fork it in a namespace you own first.

### Develop your plugin

The plugin to develop in this test should implement an physical object with spherical collision shape that displays his GNSS coordinates (latitude, longitude & altitude).

For GNSS coordinates computing, we will assume that the origin of the level is located at Humanitas Solutions office (276 Rue Saint-Jacques #300, Montreal, Quebec H2Y 1N3, Canada).

Here is a roadmap suggestion for the development:

- Create a new Unreal Engine plugin.
- Add a new actor with spherical mesh and collision shape.
- Compute the GNSS coordinates in Unreal Engine.
- Display the GNSS coordinates in a sensible manner.
- Test this new actor in a demonstration level.

You might find the instructions above a bit vague, but do not worry, this is just a chance for you to express your creativity.

One last thing, do not forget to check the [Contributing](CONTRIBUTING.md) page for further instructions and help.

### Submit your solution

Once you are satisfied with your work, please submit a merge request to the upstream repository. This merge request's description should include:

- a short description of your work (features, limitations, future improvements)
- a short video showing your plugin in action

:warning: You have only one week to submit your solution from the moment you have been given access to the in-house repositories.

## Contributing

Before starting to experiment in _UE5 Coding Test_, please refer to the [Contributing](CONTRIBUTING.md) page.

## License

This project is licensed under the Humanitas Solutions' proprietary license - see the [LICENSE](LICENSE) file for details.

## Related

- [Unreal Engine](https://www.unrealengine.com)
- [HXS Wiki](http://humanitasio.gitlab.io/hxs-wiki-public)
